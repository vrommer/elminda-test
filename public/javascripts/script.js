"use strict";

class PubSub {
	functionId = 0;
	topics = new Map();

	publish(topic, data) {
		let subscribers = this.topics.get(topic);
		if (subscribers && subscribers.size) {
			subscribers.forEach(fnSubscriber => {
				fnSubscriber(data);
			});
		}
		else {
			console.log("no subscribers for topic");
		}
	}

	subscribe(topic, callback) {
		let subscribers = this.topics.get(topic),
			subscriberIndex;
		if (!subscribers) {
			this.topics.set(topic, new Map());
			subscribers = this.topics.get(topic);
		}
		subscriberIndex = this.functionId;
		subscribers.set(this.functionId, callback);
		this.functionId++;
		return function(){
			subscribers = subscribers.delete(subscriberIndex);
		};
	}
}

const pubsub = new PubSub();

let unsubscribe1 = pubsub.subscribe('customerInformation', data => {
	console.log('customer information 1:', data);
});

let unsubscribe4 = pubsub.subscribe('customerInformation', data => {
	console.log('customer information 2:', data);
});

let unsubscribe5 = pubsub.subscribe('customerInformation', data => {
	console.log('customer information 3:', data);
});

pubsub.publish('customerInformation', {
	name: 'John Doe'
});

unsubscribe1();

pubsub.publish('customerInformation', {
	name: 'John Doe'
});

let unsubscribe2 = pubsub.subscribe('topic2', data => {
	console.log('topic2 1:', data);
});

pubsub.subscribe('topic2', data => {
	console.log('topic2 2:', data);
});

let unsubscribe3 = pubsub.subscribe('topic2', data => {
	console.log('topic2 3:', data);
});

pubsub.publish('topic2', {
	name: 'Topic 2'
});

unsubscribe2();

pubsub.publish('topic2', {
	name: 'Topic 2'
});

unsubscribe3();

pubsub.publish('topic2', {
	name: 'Topic 2'
});

unsubscribe4();

pubsub.publish('customerInformation', {
	name: 'John Doe'
});

unsubscribe5();

pubsub.publish('customerInformation', {
	name: 'John Doe'
});


